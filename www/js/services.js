/**
 * Created by jsrolon on 26-10-2016.
 */

angular.module('bogotaEats.services', [])
.factory('DataService', function ($http, $q) {

  var reviews;

  var localFavoritesList = [];

  var favoritesId;

  return {
    toggleFavorite: function (reviewId) {
      reviewId = reviewId + "";
      if (this.isLocalFavorite(reviewId)) {
        for (var i = 0; i < localFavoritesList.length; i++) {
          if (localFavoritesList[i] === reviewId) {
            localFavoritesList.splice(i, 1);
            break;
          }
        }
      } else {
        localFavoritesList.push(reviewId + "");
      }

      if (localFavoritesList.length > 0) {
        this.patchFavorites();
      } else {
        this.deleteFavorites();
      }
    },

    deleteFavorites: function () {
      $http.get('http://bogotaeatsback.mybluemix.net/api/favoritos?filter[where][idUser]='
        + JSON.parse(localStorage.getItem('profile')).email)
      .then(function (res) {
        return res.data[0];
      })
      .then(function (res) {
        $http.delete('http://bogotaeatsback.mybluemix.net/api/favoritos/' + res.id);
      });
    },

    isLocalFavorite: function (reviewId) {
      reviewId = reviewId + "";
      return localFavoritesList.reduce(function (prev, curr) {
        return prev || curr === reviewId;
      }, false);
    },

    isFavorite: function (reviewId) {
      // $http.get('http://bogotaeatsback.mybluemix.net/api/favoritos/' + reviewId).then(function (res) {
      //   console.log(res);
      //   return res;
      // });
      // return this.getFavorites().then(function(res) {
      //   return $scope.favorites.indexOf(reviewId >= 0);
      // });
    },

    patchFavorites: function () {
      if (favoritesId) {
        $http.patch('http://bogotaeatsback.mybluemix.net/api/favoritos/', {
          idUser: JSON.parse(localStorage.getItem('profile')).email,
          restaurantes: localFavoritesList,
          id: favoritesId
        });
      } else {
        $http.get('http://bogotaeatsback.mybluemix.net/api/favoritos?filter[where][idUser]='
          + JSON.parse(localStorage.getItem('profile')).email)
        .then(function (res) {
          return res.data[0];
        })
        .then(function (res) {
          if (res) {
            favoritesId = res.id;
            $http.patch('http://bogotaeatsback.mybluemix.net/api/favoritos/', {
              idUser: JSON.parse(localStorage.getItem('profile')).email,
              restaurantes: localFavoritesList,
              id: res.id
            });
          } else {
            $http.patch('http://bogotaeatsback.mybluemix.net/api/favoritos/', {
              idUser: JSON.parse(localStorage.getItem('profile')).email,
              restaurantes: localFavoritesList,
            });
          }
        });
      }
    },

    getFavorites: function () {
      var profile = JSON.parse(localStorage.getItem('profile'));
      if (profile) {
        return $http.get('http://bogotaeatsback.mybluemix.net/api/favoritos?filter[where][idUser]='
          + profile.email).then(function (res) {
          if (res.data.length > 0) {
            localFavoritesList = res.data[0].restaurantes;
            favoritesId = res.data[0].id;
            return res.data[0].restaurantes;
          } else {
            return [];
          }
        });
      } else {
        return [];
      }
    },

    getReviews: function () {
      return $http.get('http://bogotaeatsback.mybluemix.net/api/restaurantes');
    },

    getReview: function (reviewId) {
      return $http.get('http://bogotaeatsback.mybluemix.net/api/restaurantes/' + reviewId);
    },
    newReview: function (review) {
      return $http.post('http://bogotaeatsback.mybluemix.net/api/restaurantes/', review);
    },
    updateReview: function (review) {
      return $http.put('http://bogotaeatsback.mybluemix.net/api/restaurantes/', review);
    }
  }
})

.factory('DistanceService', function () {
  return {
    getDistance: function (lat1, lon1, lat2, lon2, unit) {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") {
        dist = dist * 1.609344
      }
      if (unit == "N") {
        dist = dist * 0.8684
      }
      return dist;
    }
  }
})

.factory('FilterService', function ($ionicPopup, DataService) {
  // filters object contains static categories
  var filters = {
    tipoComida: {
      name: "Tipo de Comida",
      values: [
        {name: "Americana", checked: false},
        {name: "Oriental", checked: false}
      ]
    },
    tipoOcasion: {
      name: "Ocasión",
      values: [
        {name: "Familiar", checked: false},
        {name: "Desayuno", checked: false}
      ]
    },
    zona: {
      name: "Ubicación",
      values: [
        {name: "Usaquén", checked: false}
      ]
    }
  };

  // DataService.getReviews().then(function(reviews) {
  //   angular.forEach(reviews, function(review) {
  //     angular.forEach(filters, function(obj, category) {
  //       obj.values[review[category]] = false;
  //     });
  //   });
  // });

  return {
    filters: filters
  }
})

.factory('AuthService', function ($rootScope, angularAuth0, authManager, jwtHelper, $location, $ionicPopup, $ionicHistory, $state) {
  var userProfile = JSON.parse(localStorage.getItem('profile')) || {};

  function login(username, password) {
    angularAuth0.login({
      connection: 'bogotaEats',
      responseType: 'token',
      popup: true,
      email: username,
      password: password
    }, onAuthenticated, null);
  }

  function signup(username, password, callback) {
    console.log('signing up');
    angularAuth0.signup({
      connection: 'bogotaEats',
      responseType: 'token',
      // popup: true,
      email: username,
      password: password
    }, onAuthenticated, null);
  }

  function loginWithGoogle() {
    angularAuth0.login({
      connection: 'google-oauth2',
      responseType: 'token',
      // popup: true
    }, onAuthenticated, null);
  }

  function loginWithFacebook() {
    angularAuth0.login({
      connection: 'facebook',
      responseType: 'token',
      // popup: true
    }, onAuthenticated, null);
  }

  // Logging out just requires removing the user's
  // id_token and profile
  function logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    localStorage.removeItem('authorized');
    authManager.unauthenticate();
    userProfile = {};
  }

  function authenticateAndGetProfile() {
    var result = angularAuth0.parseHash(window.location.hash);

    if (result && result.idToken) {
      onAuthenticated(null, result);
    } else if (result && result.error) {
      onAuthenticated(result.error);
    }
  }

  function onAuthenticated(error, authResult) {
    if (error) {
      return $ionicPopup.alert({
        title: 'Login failed!',
        template: error
      });
    }

    localStorage.setItem('id_token', authResult.idToken);
    authManager.authenticate();

    angularAuth0.getProfile(authResult.idToken, function (error, profileData) {
      if (error) {
        return console.log(error);
      }

      localStorage.setItem('profile', JSON.stringify(profileData));
      userProfile = profileData;

      // set authorized, which is used for non-logged in users
      localStorage.setItem('authorized', 'true');

      localStorage.setItem('tipo', profileData.user_metadata.roles[0]);

      // make sure there's not a back button on redirect
      $ionicHistory.clearHistory();
      if(profileData.user_metadata.roles[0] == "admin"){
        $state.go('admin');
      }else {
        $location.path('/');
      }

    });
  }

  function checkAuthOnRefresh() {
    var token = localStorage.getItem('id_token');
    if (token) {
      if (!jwtHelper.isTokenExpired(token)) {
        if (!$rootScope.isAuthenticated) {
          authManager.authenticate();
        }
      }
    }
  }

  function isLoggedIn() {
    return !!localStorage.getItem('id_token');
  }

  return {
    login: login,
    logout: logout,
    signup: signup,
    loginWithGoogle: loginWithGoogle,
    loginWithFacebook: loginWithFacebook,
    checkAuthOnRefresh: checkAuthOnRefresh,
    authenticateAndGetProfile: authenticateAndGetProfile,
    isLoggedIn: isLoggedIn
  }
})
;
