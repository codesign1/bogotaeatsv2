angular.module('bogotaEats.controllers', ['bogotaEats.services'])

/**
 * Controller for the SignUp template
 */
.controller('SignUpCtrl', function ($scope, AuthService, $location, $ionicHistory, $state) {
  $scope.options = {
    loop: false,
    effect: 'slide',
    speed: 500,
    pagination: false
  };

  $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
    // data.slider is the instance of Swiper
    $scope.slider = data.slider;
  });

  $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
    console.log('Slide change is beginning');
  });

  $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
    // note: the indexes are 0-based
    $scope.activeIndex = data.slider.activeIndex;
    $scope.previousIndex = data.slider.previousIndex;
  });

  // Form data for the login modal
  $scope.loginData = {};

  $scope.signUp = function () {
    AuthService.signup($scope.loginData.username, $scope.loginData.password);
  };

  $scope.logIn = function () {
    AuthService.login($scope.loginData.username, $scope.loginData.password);
  };

  $scope.loginWithFacebook = function () {
    AuthService.loginWithFacebook();
  };

  $scope.loginWithGoogle = function () {
    AuthService.loginWithGoogle();
  };

  $scope.continueWithoutRegistration = function () {
    localStorage.setItem('authorized', 'true');

    // make sure there's not a back button on redirect
    $ionicHistory.clearHistory();
    $location.path('/app/newsfeed');
  }
})

.controller('AppCtrl', function ($scope, $ionicModal, $timeout, AuthService, $location) {

  $scope.isLoggedIn = function () {
    return AuthService.isLoggedIn();
  };

  $scope.logOut = function () {
    localStorage.clear();
    $location.path('/sign-up');
  };

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:

  $scope.init = function () {
    var authorized = localStorage.getItem('authorized');
    if (!authorized) {
      $location.path('/sign-up');
    } else {
      $location.path('/');
    }
  };

  $scope.init();
})

.controller('NewsfeedCtrl', function ($scope, DataService, $location, $ionicPopup, FilterService, $ionicLoading) {
  $ionicLoading.show({
    template: 'Cargando...'
  });

  $scope.selectedOrder = 'fecha';
  $scope.reverseOrder = true;

  DataService.getFavorites();

  DataService.getReviews().then(function (res) {
    $scope.reviews = res.data;
    $ionicLoading.hide();
  });

  $scope.onSwipe = function (direction) {
    console.log(direction);
  };

  $scope.toggleFavorite = function (reviewId) {
    DataService.toggleFavorite(reviewId);
  };

  $scope.isFavorite = function (reviewId) {
    return DataService.isFavorite(reviewId);
  };

  $scope.selectOrder = function (order) {
    if ($scope.selectedOrder === order) {
      $scope.reverseOrder = !$scope.reverseOrder;
    } else {
      $scope.selectedOrder = order;
      $scope.reverseOrder = true;
    }
  };

  $scope.openMap = function () {
    $location.path('/app/map')
  };

  /**
   * Filter popup related
   */

  $scope.showFilters = function () {
    $scope.filterPopup = $ionicPopup.show({
      title: 'Filtrar',
      templateUrl: 'templates/popups/filters.html',
      cssClass: 'filters-popup',
      scope: $scope
    });
  };

  $scope.hideFilters = function () {
    $scope.filterPopup.close();
  };

  $scope.filters = FilterService.filters;

  $scope.toggleGroup = function (group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function (group) {
    return $scope.shownGroup === group;
  };
})

.controller('MapCtrl', function ($scope, DataService, $cordovaGeolocation, $ionicPopup, FilterService,
                                 $ionicLoading, $filter, $location, $timeout) {
  $scope.filters = FilterService.filters;

  $scope.$watch('filters', function (newVal) {
    $scope.reviews = $filter('bogotaEats')($scope.realReviews);
    $timeout(function() {
      $scope.calculateCarouselReviews();
    });
    $scope.addMarkers($scope.reviews);
  }, true);

  $scope.mapMarkers = {};

  // the displayReviews will change depending on what the user selected as a location filter
  $scope.selectedLocation = '';
  $scope.currentPosition = null;

  $scope.carouselReviews = [];

  $scope.zones = {};

  $scope.$on('$ionicView.enter', function (e) {
    if(!$scope.realReviews) {
      $ionicLoading.show({
        template: 'Cargando...'
      });

      Promise.all([DataService.getReviews(), $cordovaGeolocation.getCurrentPosition()])
      .then(function (res) {
        $scope.realReviews = res[0].data;
        $scope.reviews = $filter('bogotaEats')($scope.realReviews);

        angular.forEach($scope.reviews, function (review) {
          $scope.zones[review.zona] = true;
        });
        $scope.$apply(function () {
          $scope.zones = Object.keys($scope.zones);
        });

        $scope.currentPosition = res[1].coords;

        var mapOptions = {
          zoom: 15,
          center: {lat: $scope.currentPosition.latitude, lng: $scope.currentPosition.longitude},
          disableDefaultUI: true
        };

        $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

        $scope.calculateCarouselReviews($scope.reviews);

        // add a listener for when the bounds change, so that we can check which reviews are in view and show them in
        // the bottom carousel
        $scope.map.addListener('bounds_changed', function () {
          $scope.calculateCarouselReviews();
        });

        $scope.addMarkers($scope.reviews);
        $ionicLoading.hide();
      });
    }
  });

  $scope.addMarkers = function (reviews) {
    // clear the map reference on all existing markers
    angular.forEach($scope.mapMarkers, function (marker) {
      marker.setMap(null);
    });
    $scope.mapMarkers = {};

    angular.forEach(reviews, function (review) {
      $scope.mapMarkers[review.id] = new google.maps.Marker({
        position: {lat: review.geoloc.lat, lng: review.geoloc.lng},
        map: $scope.map,
        title: 'Hello World! ' + review.id
      });
    });
  };

  $scope.calculateCarouselReviews = function () {
    if ($scope.map) {
      var bounds = $scope.map.getBounds();
      if(bounds) {
        $scope.$apply(function () {
          $scope.carouselReviews = [];
          angular.forEach($scope.reviews, function (review) {
            var isInsideBounds = bounds.contains(new google.maps.LatLng(review.geoloc.lat, review.geoloc.lng));
            if (isInsideBounds) {
              $scope.carouselReviews.push(review);
            }
          });
        });
      }
    }
  };

  $scope.navigateToReview = function (reviewId) {
    $location.path('/app/review/' + reviewId);
  };

  $scope.moveMapTo = function (selected) {
      // calculate the new center of the map and assign it
      var coordinates = [];
      angular.forEach($scope.reviews, function (review) {
        if (review.zona === selected.name) {
          coordinates.push({
            latitude: review.geoloc.lat,
            longitude: review.geoloc.lng
          })
        }
      });
      var newCenter = geolib.getCenter(coordinates);

      // this one triggers a bounds change, which calls calculateCarouselReviews
      $scope.map.setCenter(new google.maps.LatLng(newCenter.latitude, newCenter.longitude));
  };

  /**
   * Filter popup related
   */

  $scope.showFilters = function () {
    $scope.filterPopup = $ionicPopup.show({
      title: 'Filtrar',
      templateUrl: 'templates/popups/filters.html',
      cssClass: 'filters-popup',
      scope: $scope
    });
  };

  $scope.hideFilters = function () {
    $scope.filterPopup.close();
  };

  $scope.filters = FilterService.filters;

  $scope.toggleGroup = function (group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function (group) {
    return $scope.shownGroup === group;
  };
})

.controller('FavoritosCtrl', function ($scope, DataService) {
  $scope.isFavorites = true;

  $scope.$on('$ionicView.enter', function (e) {
    Promise.all([DataService.getFavorites(), DataService.getReviews()])
    .then(function (res) {
      var favorites = res[0];
      var reviews = res[1].data;
      $scope.$apply(function () {
        $scope.favorites = [];
        for (var i = 0; i < reviews.length; i++) {
          var reviewId = reviews[i].id + "";
          if (favorites.indexOf(reviewId) >= 0) {
            $scope.favorites.push(reviews[i]);
          }
        }
      });
    });
  });
})

.controller('ReviewCtrl', function ($scope, $stateParams, DataService, AuthService, $http,
                                    $ionicModal, $ionicPopup, $ionicLoading) {
    $ionicModal.fromTemplateUrl('templates/modals/review-map.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.mapModal = modal;
    });

    $scope.openMap = function () {
      $scope.mapModal.show();
      $ionicLoading.show({
        template: 'Cargando...'
      });

      var mapOptions = {
        zoom: 15,
        center: {lat: $scope.review.geoloc.lat, lng: $scope.review.geoloc.lng},
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          mapTypeIds: ['roadmap', 'terrain']
        }
      };

      $scope.map = null;
      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      var marker = new google.maps.Marker({
        position: {lat: $scope.review.geoloc.lat, lng: $scope.review.geoloc.lng},
        map: $scope.map,
        title: 'Hello World!'
      });

      $ionicLoading.hide();
    };

    $scope.closeMap = function () {
      $scope.mapModal.hide();
    };

    $scope.isLocalFavorite = function (reviewId) {
      return DataService.isLocalFavorite(reviewId);
    };

    DataService.getReview($stateParams.reviewId).then(function (res) {
      var e = res.data;
      e.calUsersTotal = (e.calUsers.reduce(function (prev, curr) {
        return prev + curr;
      }, 0) / e.calUsers.length).toFixed(1);

      e.priceString = "$".repeat(e.precio);

      $scope.review = e;
    });

    $scope.$on('$ionicView.enter', function (e) {
      DISQUS.reset({
        reload: true,
        config: function () {
          console.log(window.location.href);
          this.page.url = window.location.href;  // Replace PAGE_URL with your page's canonical URL variable
          this.page.identifier = random(); // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        }
      });
    });

    $scope.sliderOptions = {
      range: {min: 0, max: 10},
      step: 1
    };

    $scope.sliderEventHandlers = {
      set: function (values, handle, unencoded) {
        $scope.userGrade = parseInt(values);
        console.log($scope.userGrade);
      }
    };

    $scope.isLoggedIn = function () {
      return AuthService.isLoggedIn();
    };

    $scope.toggleFavorite = function (reviewId) {
      if($scope.isLoggedIn()) {
        DataService.toggleFavorite(reviewId);
      } else {
        $ionicPopup.alert({
          title: 'Iniciar sesión',
          template: 'Debes iniciar sesión para poder agregar a favoritos.'
        });
      }
    };

    $scope.isFavorite = function (reviewId) {
      return DataService.isFavorite(reviewId);
    };

    $scope.sendUserGrade = function () {
      var reviewWithNewGrade = $scope.review;
      reviewWithNewGrade.calUsers.push($scope.userGrade);
      $http.patch('http://bogotaeats.mybluemix.net/api/restaurantes/', reviewWithNewGrade)
      .then(function (res) {
        $scope.review.calUsersTotal = (res.data.calUsers.reduce(function (prev, curr) {
          return prev + curr;
        }, 0) / res.data.calUsers.length).toFixed(1);

        $ionicPopup.alert({
          title: 'Calificación guardada',
          template: 'Gracias! Tu calificación ha sido guardada.'
        });
      });
    };
  }
)

.controller('adminCtrl', function ($scope, $state, $ionicHistory) {

})

.controller('reviewsCtrl', function ($scope, $ionicHistory, $stateParams, DataService, $cordovaCamera) {
  $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };
  $scope.foto = false;
  $scope.fotos = [];

  if($stateParams.idReview){
    DataService.getReview($stateParams.idReview)
      .then(function (data) {
        $scope.review = data.data;
        console.log(data.data);
        $scope.fotos = $scope.review.fotos;
        console.log($scope.fotos);
        $scope.foto = true;
      });
    $scope.updating = true;
  }

  $scope.review = {
    nombre: null,
    zona: null,
    direccion: null,
    geoloc: {
      lat: null,
      lng: null,
    },
    comentario: null,
    fecha: new Date(),
    precio: null,
    calificacion: null,
    calUsers:[0],
    fotos:[],
    platos:[
      {}
    ],
    tipoComida: null,
    tipoOcasion: null
  };

  $scope.picture = function () {
    console.log('foto');
    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 500,
      targetHeight: 500,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.img = imageData;
      var fot = "data:image/jpeg;base64," + imageData;
      $scope.fotos.push(fot);
      $scope.foto = true;
    }, function (err) {
      console.log(err);
    });
  };

  $scope.saveReview = function (review) {
    console.log(review);
    review.fotos = $scope.fotos;
    DataService.newReview(review);
  };

  $scope.updateReview = function (review) {
    DataService.updateReview(review);
  }
})

  .controller('editableCtrl', function ($scope, DataService, $location, $ionicPopup, FilterService, $ionicLoading) {
    $ionicLoading.show({
      template: 'Cargando...'
    });

    $scope.selectedOrder = 'fecha';
    $scope.reverseOrder = true;

    DataService.getReviews().then(function (res) {
      $scope.reviews = res.data;
      console.log(res);
      $ionicLoading.hide();
    });

    $scope.onSwipe = function (direction) {
      console.log(direction);
    }
  })
;

