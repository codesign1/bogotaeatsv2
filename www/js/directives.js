/**
 * Created by jsrolon on 18-11-2016.
 */

angular.module('bogotaEats.controllers')
.directive('newsfeedItem', function ($cordovaGeolocation, DistanceService, DataService, AuthService) {
  return {
    scope: {
      review: '=',
      isFavorites: '='
    },
    restrict: 'AE',
    replace: true,
    templateUrl: 'templates/directives/newsfeed-item.html',
    link: function (scope, elem, attrs) {
      // calculate the average, it was either this or directives
      scope.calUsersTotal = (scope.review.calUsers.reduce(function(prev, curr) {
        return prev + curr;
      }, 0) / scope.review.calUsers.length).toFixed(1);

      // price in number of $s
      scope.priceString = "$".repeat(scope.review.precio);

      $cordovaGeolocation.getCurrentPosition().then(function (res) {
        scope.distanceTo = parseInt(DistanceService.getDistance(res.coords.latitude, res.coords.longitude,
              scope.review.geoloc.lat, scope.review.geoloc.lng, 'K') * 1000) + 'm';
      });

      scope.toggleFavorite = function(reviewId) {
        DataService.toggleFavorite(reviewId);
      };

      scope.isLoggedIn = function() {
        return AuthService.isLoggedIn();
      };

      scope.isLocalFavorite = function(reviewId) {
        return DataService.isLocalFavorite(reviewId);
      }
    }
  };
})

  .directive('newsfeededitableItem', function ($cordovaGeolocation, DistanceService, DataService, AuthService) {
    return {
      scope: {
        review: '=',
        isFavorites: '='
      },
      restrict: 'AE',
      replace: true,
      templateUrl: 'templates/directives/newsfeed-editableitem.html',
      link: function (scope, elem, attrs) {
        // calculate the average, it was either this or directives
        scope.calUsersTotal = (scope.review.calUsers.reduce(function(prev, curr) {
          return prev + curr;
        }, 0) / scope.review.calUsers.length).toFixed(1);

        // price in number of $s
        scope.priceString = "$".repeat(scope.review.precio);

        $cordovaGeolocation.getCurrentPosition().then(function (res) {
          scope.distanceTo = parseInt(DistanceService.getDistance(res.coords.latitude, res.coords.longitude,
                scope.review.geoloc.lat, scope.review.geoloc.lng, 'K') * 1000) + 'm';
        });

        scope.toggleFavorite = function(reviewId) {
          DataService.toggleFavorite(reviewId);
        };

        scope.isLoggedIn = function() {
          return AuthService.isLoggedIn();
        };

        scope.isLocalFavorite = function(reviewId) {
          return DataService.isLocalFavorite(reviewId);
        }
      }
    };
  })
;
